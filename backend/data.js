import bcrypt from 'bcryptjs';

const data = {
  users: [
    {
      name: 'Jear',
      email: 'admin@example.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: true,
    },
    {
      name: 'John',
      email: 'user@example.com',
      password: bcrypt.hashSync('123456'),
      isAdmin: false,
    },
  ],
  products: [
    {
      // _id: '1',
      name: 'EVO carbon full face helmet',
      slug: 'EVO-Helmet',
      category: 'Helmet',
      image: '/images/p1.jpg', // 679px × 829px
      price: 120,
      countInStock: 10,
      brand: 'EVO',
      rating: 4.5,
      numReviews: 3,
      description: 'high quality helmet',
    },
    {
      // _id: '2',
      name: 'HJC full face helmet',
      slug: 'HJC-Helmet',
      category: 'Helmet',
      image: '/images/p2.jpg',
      price: 250,
      countInStock: 0,
      brand: 'HJC',
      rating: 4.0,
      numReviews: 10,
      description: 'high quality helmet',
    },
    {
      // _id: '3',
      name: 'Imprint customs sleeves BNW',
      slug: 'Imprint customs sleeves',
      category: 'Sleeves',
      image: '/images/p3.jpg',
      price: 25,
      countInStock: 15,
      brand: 'Imprint customs',
      rating: 4.5,
      numReviews: 14,
      description: 'high quality product',
    },
    {
      // _id: '4',
      name: 'Imprint customs sleeves',
      slug: 'Imprint customs sleeves',
      category: 'Sleeves',
      image: '/images/p4.jpg',
      price: 65,
      countInStock: 5,
      brand: 'Imprint customs',
      rating: 4.5,
      numReviews: 10,
      description: 'high quality product',
    },
  ],
};
export default data;
